-- Alpine Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Ghc = ../components/Ghc.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "autoconf"
    , "automake"
    , "bash"
    , "binutils-gold"
    , "build-base"
    , "coreutils"
    , "cpio"
    , "curl"
    , "gcc"
    , "git"
    , "gmp"
    , "gmp-dev"
    , "grep"
    , "libffi"
    , "libffi-dev"
    , "linux-headers"
    , "llvm10"
    , "lld"
    , "musl-dev"
    , "ncurses-dev"
    , "ncurses-libs"
    , "ncurses-static"
    , "perl"
    , "python3"
    , "sudo"
    , "wget"
    , "xz"
    , "zlib-dev"
    ]

let
  docsBuildDepends: List Text =
    [ "py3-sphinx"
    , "texlive"
    , "texlive-xetex"
    , "texmf-dist-latexextra"
    , "ttf-dejavu"
    ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let installDepsStep: CF.Type =
      CF.run "Installing GHC build dependencies"
      [ "apk add --no-cache ${Prelude.Text.concatSep " " buildDepends}" ]

let
  ghcVersion: Text = "9.2.2"

let
  fetchGhcStep: Ghc.Bindist -> CF.Type =
    \(ghcBindist: Ghc.Bindist) ->
      let
        destDir: Text = "/opt/ghc/${ghcVersion}"
      in 
        Ghc.install
          { bindist = ghcBindist
          , destDir = destDir
          , configureOpts = ["--disable-ld-override"] : List Text
          }
        # CF.env (toMap { GHC = "${destDir}/bin/ghc" })

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

let
  images: List Image.Type =
  [ Image::
    { name = "x86_64-linux-alpine3_12"
    , runnerTags = [ "x86_64-linux" ]
    , image = 
        CF.from "alpine:3.12.0"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installDepsStep
      # fetchGhcStep (Ghc.Bindist.BindistSpec { version = ghcVersion, triple = "x86_64-alpine3.12-linux-gmp" })
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-x86_64-linux-alpine-static.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    },
    Image ::
    { name = "i386-linux-alpine3_12"
    , runnerTags = [ "x86_64-linux" ]
    , image = 
        CF.from "i386/alpine:3.12.0"
      # [ CF.Statement.Shell ["/bin/ash", "-eo", "pipefail", "-c"] ]
      # installDepsStep
      # fetchGhcStep (Ghc.Bindist.BindistURL "https://downloads.haskell.org/~ghcup/unofficial-bindists/ghc/9.0.1/ghc-9.0.1-i386-alpine-linux.tar.xz")
      # Cabal.install (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
      # CF.run "update cabal index" [ "$CABAL update"]
      # HaskellTools.installGhcBuildDeps
      # createUserStep
      # [ CF.Statement.Cmd ["bash"] ]
    }
  ]

in images
