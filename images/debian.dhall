-- Debian/Ubuntu Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Image = ../Image.dhall

let 
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let
  coreBuildDepends: List Text =
    [ "zlib1g-dev"
    , "libtinfo-dev"
    , "libsqlite3-0"
    , "libsqlite3-dev"
    , "libgmp-dev"
    , "libncurses-dev"
    , "ca-certificates"
    , "g++"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz-utils"
    , "lbzip2"
    , "bzip2"
    , "patch"
    , "openssh-client"
    , "sudo"
    , "time"
    , "jq"
    , "wget"
    , "curl"
    , "locales"
    -- For LLVM
    , "libtinfo5"
    -- DWARF libraries
    , "libdw1", "libdw-dev"
    -- For nofib
    , "valgrind"
    -- For the dtrace code generator and headers
    , "systemtap-sdt-dev"
    -- For lndir
    , "xutils-dev"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-sphinx"
    , "texlive-xetex"
    , "texlive-latex-extra"
    , "texlive-binaries"
    , "texlive-fonts-recommended"
    , "lmodern"
    ]

let debianBuildDepends: List Text =
    [ "texlive-generic-extra" ]

let ubuntuBuildDepends: List Text =
    [ "texlive-plain-generic" ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
        CF.arg (toMap { DEBIAN_FRONTEND = "noninteractive" })
      # CF.run "install build dependencies"
        [ "apt-get update"
        , "apt-get install --no-install-recommends -qy ${Prelude.Text.concatSep " " pkgs}"
        ]

let
  cleanApt: CF.Type =
      CF.run "clean apt cache"
      [ "apt-get clean"
      , "rm -rf /var/lib/apt/lists/*"
      ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

-- Install stack for testing hadrian
let
  installStack: CF.Type =
      CF.run "install stack"
      [ "curl -sSL https://get.haskellstack.org/ | sh"
      ]

let
  DebianImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.Source
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Source
        , cabalSource : Cabal.Type
        , extraPackages: List Text
          -- N.B. Optional as Stack's installer does not support all platforms
        , withStack : Bool
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootLlvmDir: Text = "/opt/llvm-bootstrap"
        let
          llvmDir: Text = "/opt/llvm"
        let
          bootstrapLlvmConfigureOptions =
            merge { Some = \(_: Llvm.Source) -> [ "LLC=${bootLlvmDir}/bin/llc", "OPT=${bootLlvmDir}/bin/opt" ]
                  , None = [] : List Text
                  } opts.bootstrapLlvm
            
        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # installPackages (buildDepends # opts.extraPackages)
          # (if opts.withStack then installStack else [] : CF.Type)
          # cleanApt

            -- install LLVM for bootstrap GHC
          # Llvm.maybeInstallTo bootLlvmDir opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.maybeInstallTo llvmDir opts.llvm
          # Llvm.setEnv llvmDir

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # CF.run "update cabal index" [ "$CABAL update"]
          # HaskellTools.installGhcBuildDeps

          # createUserStep
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , default =
      { withStack = True
      }
    , toDocker = toDocker
    }

let debian11Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb11"
    , fromImage = "amd64/debian:bullseye"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.2.2", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          ubuntuBuildDepends
        -- For cross-compilation testing
        # ["crossbuild-essential-arm64"]
        # [ "libnuma-dev" ]
    }

, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb11"
    , fromImage = "arm32v7/debian:bullseye"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1", triple = "armv7a-linux-gnueabihf" })
    , bootstrapGhc = { version = "9.2.2", triple = "armv7-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "armv7a-linux-gnueabihf" })
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev" ] # ubuntuBuildDepends
    , withStack = False
    }
, DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb11"
    , fromImage = "arm64v8/debian:bullseye"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1" , triple = "aarch64-linux-gnu" })
    , bootstrapGhc = { version = "9.2.2", triple = "aarch64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "aarch64-linux-gnu" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ "libnuma-dev" ] : List Text
    , withStack = False
    }
]

let debian10Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb10"
    , fromImage = "arm64v8/debian:buster"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1" , triple = "aarch64-linux-gnu" })
    , bootstrapGhc = { version = "9.2.2", triple = "aarch64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "aarch64-linux-gnu" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ "libnuma-dev" ] : List Text
    , withStack = False
    }
, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb10"
    , fromImage = "arm32v7/debian:buster"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1", triple = "armv7a-linux-gnueabihf" })
    , bootstrapGhc = { version = "9.2.2", triple = "armv7-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "armv7a-linux-gnueabihf" })
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev" ] # debianBuildDepends
    , withStack = False
    }
    -- N.B. Need GHC bindist for deb10 i386
--, DebianImage.toDocker DebianImage::
--    { name = "i386-linux-deb10"
--    , fromImage = "i386/debian:buster"
--    , runnerTags = [ "x86_64-linux" ]
--    , bootstrapLlvm = None Llvm.Source
--    , bootstrapGhc = { version = "9.2.2", triple = "i386-deb10-linux" }
--    , llvm = None Llvm.Source
--    , cabalSource = Cabal.Type.FromSource "3.2.0.0"
--    , extraPackages =
--        debianBuildDepends
--      # [ "libnuma-dev" ]
--      # [ "cabal-install" ]
--    , withStack = False
--    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.2.2", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # ["crossbuild-essential-arm64"]
        # [ "libnuma-dev" ]
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10-ghc9_0"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = Some (Llvm.Source.FromBindist { version = "9.0.1", triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , bootstrapGhc = { version = "9.0.2", triple = "x86_64-deb10-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # ["crossbuild-essential-arm64"]
        # [ "libnuma-dev" ]
    }
]

let debian9Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb9"
    , fromImage = "amd64/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.2.2", triple = "x86_64-deb9-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "10.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = debianBuildDepends : List Text
    }
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb9"
    , fromImage = "i386/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.2.2", triple = "i386-deb9-linux" }
    , llvm = None Llvm.Source
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-i386-linux-alpine.tar.xz")
    , extraPackages = debianBuildDepends # [ "cabal-install" ] : List Text
    , withStack = False
    }
]

let ubuntuImages: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu20_04"
    , fromImage = "amd64/ubuntu:20.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.Source
    , bootstrapGhc = { version = "9.2.2", triple = "x86_64-deb9-linux" }
    , llvm = Some (Llvm.Source.FromBindist { version = "11.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" })
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.6.2.0", triple = "x86_64-linux-alpine-static" }
    , extraPackages = ubuntuBuildDepends
    }
]

let linterImages: List Image.Type =
[ Image::
  { name = "linters"
  , runnerTags = [ "x86_64-linux" ]
  , jobStage = "build-derived"
  , needs = [ "x86_64-linux-deb10-ghc9_0" ]
  , image =
      let installMypy: CF.Type =
          installPackages [ "python3-pip" ]
        # CF.run "installing mypy" [ "pip3 install mypy==0.701" ]

      let lintersCommit: Text = "b376b0c20e033e71751fe2059daba1ba40b886be"
      let installShellcheck: CF.Type =
          installPackages [ "shellcheck" ]
      in
          CF.from "${docker_base_url}/x86_64-linux-deb10-ghc9_0:latest"
        # [ CF.Statement.User "root" ]
        # installMypy
        # installShellcheck
        # HaskellTools.installHLint
        # [CF.Statement.User "ghc"]
  }
]

let allImages: List Image.Type =
  linterImages # debian11Images # debian10Images # debian9Images # ubuntuImages

in allImages
