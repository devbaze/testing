DHALL ?= dhall
gitlab-pipeline.yaml: gitlab-pipeline.dhall
	${DHALL} text --file=$< > $@

dockerfiles: dockerfiles.dhall
	mkdir -p dockerfiles
	${DHALL} to-directory-tree --file=$< --output=$@

