let
  Prelude = ./deps/Prelude.dhall
let
  Image = ./Image.dhall

let
  all-images: List Image.Type =
    Prelude.List.concat Image.Type
    [ ./images/alpine.dhall
    , ./images/debian.dhall
    , ./images/centos.dhall
    , ./images/fedora.dhall
    ]
in all-images
